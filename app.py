from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    foo = 'foo'
    return 'Flask App: ' + os.environ.get('FLASK_APP')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
